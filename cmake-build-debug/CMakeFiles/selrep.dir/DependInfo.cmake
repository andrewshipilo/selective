# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/andrew/CLionProjects/selrep/Recipient.cpp" "/home/andrew/CLionProjects/selrep/cmake-build-debug/CMakeFiles/selrep.dir/Recipient.cpp.o"
  "/home/andrew/CLionProjects/selrep/Sender.cpp" "/home/andrew/CLionProjects/selrep/cmake-build-debug/CMakeFiles/selrep.dir/Sender.cpp.o"
  "/home/andrew/CLionProjects/selrep/main.cpp" "/home/andrew/CLionProjects/selrep/cmake-build-debug/CMakeFiles/selrep.dir/main.cpp.o"
  "/home/andrew/CLionProjects/selrep/qcustomplot.cpp" "/home/andrew/CLionProjects/selrep/cmake-build-debug/CMakeFiles/selrep.dir/qcustomplot.cpp.o"
  "/home/andrew/CLionProjects/selrep/cmake-build-debug/selrep_autogen/mocs_compilation.cpp" "/home/andrew/CLionProjects/selrep/cmake-build-debug/CMakeFiles/selrep.dir/selrep_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_MULTIMEDIA_LIB"
  "QT_NETWORK_LIB"
  "QT_PRINTSUPPORT_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "selrep_autogen/include"
  "/home/andrew/Qt/5.10.0/gcc_64/include"
  "/home/andrew/Qt/5.10.0/gcc_64/include/QtWidgets"
  "/home/andrew/Qt/5.10.0/gcc_64/include/QtGui"
  "/home/andrew/Qt/5.10.0/gcc_64/include/QtCore"
  "/home/andrew/Qt/5.10.0/gcc_64/./mkspecs/linux-g++"
  "/home/andrew/Qt/5.10.0/gcc_64/include/QtMultimedia"
  "/home/andrew/Qt/5.10.0/gcc_64/include/QtNetwork"
  "/home/andrew/Qt/5.10.0/gcc_64/include/QtPrintSupport"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
