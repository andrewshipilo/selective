# Meta
set(AM_MULTI_CONFIG "SINGLE")
# Directories and files
set(AM_CMAKE_BINARY_DIR "/home/andrew/CLionProjects/selrep/cmake-build-debug/")
set(AM_CMAKE_SOURCE_DIR "/home/andrew/CLionProjects/selrep/")
set(AM_CMAKE_CURRENT_SOURCE_DIR "/home/andrew/CLionProjects/selrep/")
set(AM_CMAKE_CURRENT_BINARY_DIR "/home/andrew/CLionProjects/selrep/cmake-build-debug/")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "/home/andrew/CLionProjects/selrep/cmake-build-debug/selrep_autogen")
set(AM_SOURCES "/home/andrew/CLionProjects/selrep/Recipient.cpp;/home/andrew/CLionProjects/selrep/Sender.cpp;/home/andrew/CLionProjects/selrep/main.cpp;/home/andrew/CLionProjects/selrep/qcustomplot.cpp")
set(AM_HEADERS "/home/andrew/CLionProjects/selrep/Recipient.h;/home/andrew/CLionProjects/selrep/Sender.h;/home/andrew/CLionProjects/selrep/qcustomplot.h")
# Qt environment
set(AM_QT_VERSION_MAJOR "5")
set(AM_QT_VERSION_MINOR "10")
set(AM_QT_MOC_EXECUTABLE "/home/andrew/Qt/5.10.0/gcc_64/bin/moc")
set(AM_QT_UIC_EXECUTABLE )
set(AM_QT_RCC_EXECUTABLE )
# MOC settings
set(AM_MOC_SKIP "/home/andrew/CLionProjects/selrep/cmake-build-debug/selrep_autogen/mocs_compilation.cpp")
set(AM_MOC_DEFINITIONS "QT_CORE_LIB;QT_GUI_LIB;QT_MULTIMEDIA_LIB;QT_NETWORK_LIB;QT_PRINTSUPPORT_LIB;QT_WIDGETS_LIB")
set(AM_MOC_INCLUDES "/home/andrew/CLionProjects/selrep/cmake-build-debug;/home/andrew/CLionProjects/selrep;/home/andrew/CLionProjects/selrep/cmake-build-debug/selrep_autogen/include;/home/andrew/Qt/5.10.0/gcc_64/include;/home/andrew/Qt/5.10.0/gcc_64/include/QtWidgets;/home/andrew/Qt/5.10.0/gcc_64/include/QtGui;/home/andrew/Qt/5.10.0/gcc_64/include/QtCore;/home/andrew/Qt/5.10.0/gcc_64/./mkspecs/linux-g++;/home/andrew/Qt/5.10.0/gcc_64/include/QtMultimedia;/home/andrew/Qt/5.10.0/gcc_64/include/QtNetwork;/home/andrew/Qt/5.10.0/gcc_64/include/QtPrintSupport;/usr/include")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "FALSE")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "/usr/bin/c++;-dM;-E;-c;/home/andrew/Downloads/clion-2018.1.1/clion-2018.1.1/bin/cmake/share/cmake-3.10/Modules/CMakeCXXCompilerABI.cpp")
# UIC settings
set(AM_UIC_SKIP )
set(AM_UIC_TARGET_OPTIONS )
set(AM_UIC_OPTIONS_FILES )
set(AM_UIC_OPTIONS_OPTIONS )
set(AM_UIC_SEARCH_PATHS )
# RCC settings
set(AM_RCC_SOURCES )
set(AM_RCC_BUILDS )
set(AM_RCC_OPTIONS )
set(AM_RCC_INPUTS )
# Configurations options
set(AM_CONFIG_SUFFIX_Debug "_Debug")
