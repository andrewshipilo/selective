
#include "Sender.h"

int Sender::ReceiveReceipt(int receipt)
{
    int response = current;
    current += 1;

    if (receipt < 0)
    {
        response = -receipt;
        current -= 1;
    }
    else if (current > M + 1)
    {
        response = 0;
    }

    return response;
}
