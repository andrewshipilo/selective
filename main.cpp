#include <iostream>
#include <random>
#include <queue>
#include <QtGui/QColor>
#include <qcustomplot.h>
#include "Recipient.h"
#include "Sender.h"

std::vector<double> Imitate(double p, int tau, int l, int msgNum);
void Plot(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot, QColor color, int plotNum,
          const std::string &plotName);

std::vector<double> Imitate(double p, int tau, int l, int msgNum)
{
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_real_distribution<float> dist(0.f, 1.f);

    std::queue<int> strChannel;
    std::queue<int> retChannel;
    strChannel.push(0);

    for (int i = 0; i < tau; i++)
    {
        retChannel.push(0);
    }

    int T = 0;
    std::vector<int> buffer;

    auto sender = Sender(msgNum, tau);
    auto recipient = Recipient(l);
    std::vector<int> Ns;
    int N = 1;
    int N_sum = 0;

    while (recipient.WaitingFor < msgNum + 1)
    {
        T += 1;
        //printf("\nT = %d", T);

        auto receiptArrived = retChannel.front();
        retChannel.pop();

        auto messageToGo = sender.ReceiveReceipt(receiptArrived);

        auto messageArrived = strChannel.front();
        strChannel.pop();

        //auto receiptToGo = recipient.ReceiveMessage(messageArrived);
        //auto receiptToGo = recipient.ReceiveMessageAlternative(messageArrived);
        auto receiptToGo = recipient.ReceiveMessageFlex(messageArrived);

        if (receiptToGo > 0)
        {
            Ns.push_back(N);
            N_sum += N;
            N = 1;
        } else if (receiptToGo < 0)
        {
            N++;
        }

        if (dist(rng) < p)
        {
            messageToGo = (-1) * messageToGo;
        }
        strChannel.push(messageToGo);
        retChannel.push(receiptToGo);
    }

    return { msgNum / (double) T, N_sum / (double) Ns.size() };
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    std::vector<int> L = { 2, 5, 7, 11 };
    std::vector<int> Tau = { 2, 3, 4, 5 };

    for (auto& l : L)
    {
        std::cout << "L: " << l << std::endl;
        QCustomPlot* plot1 = new QCustomPlot();
        QCustomPlot* plot2 = new QCustomPlot();
        int cnt1 = 0;
        int cnt2 = 0;
        for (auto tau : Tau)
        {
            //std::cout << "\tTau: " << tau << std::endl;
            std::vector<double> kpd;
            std::vector<double> p;
            std::vector<double> Ns;

            for (double i = 0.0; i < 0.9; i += 0.1)
            {
                //std::cout << "\t P: " << i << std::endl;
                auto kol = l == 13 ? 1000000 : 10000;
                auto res = Imitate(i, tau, l, kol);
                kpd.push_back(res[0]);
                Ns.push_back(res[1]);
                p.push_back(i);
            }
            std::string name = "tau = " + std::to_string(tau);
            Plot(p, kpd, plot1, Qt::GlobalColor(Qt::red + cnt1), cnt1, name);
            Plot(p, Ns, plot2, Qt::GlobalColor(Qt::red + cnt2), cnt2, name);
            cnt1++;
            cnt2++;
            if (l == 13)
            {
                std::cout << name << std::endl;
                std::cout << "[ ";
                for (auto& val : kpd)
                {
                    std::cout << val << " ";
                }
                std::cout << " ]" << std::endl;
            }
        }
        if (l == 13) {
            std::cout << "fun 1 - x: " << std::endl;
            std::cout << "[ ";

            for (double i = 0.0; i < 0.9; i += 0.1) {
                std::cout << (1 - i) << " ";
            }
            std::cout << " ]" << std::endl;
        }

        std::string lstr = std::to_string(l);
        plot1->savePng("Graphs/Kpd(p) L = " + QString::fromStdString(lstr) + ".png");
        plot2->savePng("Graphs/Avg(p) L = " + QString::fromStdString(lstr) + ".png");


        QCustomPlot* plot3 = new QCustomPlot();
        QCustomPlot* plot4 = new QCustomPlot();
        int cnt3 = 0;
        int cnt4 = 0;
        for (double i = 0.1; i < 0.9; i += 0.1)
        {
            std::vector<double> kpd;
            std::vector<double> p;
            std::vector<double> Ns;
            std::vector<double> taus;

            for (int t = 2; t < 5; t++)
            {
                auto res = Imitate(i, t, l, 10000);
                kpd.push_back(res[0]);
                Ns.push_back(res[1]);
                p.push_back(i);
                taus.push_back(t);
            }
            std::string name = "p = " + std::to_string(i);
            Plot(taus, kpd, plot3, Qt::GlobalColor(Qt::red + cnt3), cnt3, name);
            Plot(taus, Ns, plot4, Qt::GlobalColor(Qt::red + cnt4), cnt4, name);
            cnt3++;
            cnt4++;
        }
        plot3->savePng("Graphs/Kpd(tau) L = " + QString::fromStdString(lstr) + ".png");
        plot4->savePng("Graphs/Avg(tau) L = " + QString::fromStdString(lstr) + ".png");

        delete plot1;
        delete plot2;
        delete plot3;
        delete plot4;
    }

    /*double P = 0.3;
    int tau = 5;
    int L = 4;
    int msgNum = 100;

    Imitate(P, tau, L, msgNum);
    */
    return 0;
}

void Plot(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot, QColor color, int plotNum,
          const std::string &plotName)
{
    QVector<double> X = QVector<double>::fromStdVector(x);
    QVector<double> Y = QVector<double>::fromStdVector(y);

    plot->resize(512, 512);
    plot->legend->setVisible(true);
    plot->legend->setFont(QFont("Helvetica", 9));
    plot->addGraph();
    plot->graph(plotNum)->setName(QString::fromStdString(plotName));
    plot->graph(plotNum)->setPen(QPen(color));
    plot->graph(plotNum)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    plot->graph(plotNum)->setData(X, Y);

    plot->rescaleAxes();
    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}