#ifndef SELREP_SENDER_H
#define SELREP_SENDER_H

class Sender
{
    int M;
    int tau;
    int current = 1;

public:
    Sender(int _M, int _tau) : M(_M), tau(_tau) {}

    int ReceiveReceipt(int receipt);
};


#endif
