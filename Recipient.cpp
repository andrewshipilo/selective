#include "Recipient.h"
inline long mod(long a, long b)
{
    return (a % b + b) % b;
}

int Recipient::ReceiveMessage(int message)
{
    int response = 0;

    if (message < 0)
    {
        response = message;
    }
    else if (message != WaitingFor)
    {
        if (buffer.size() == L || message > WaitingFor + L)
        {
            response = -message;
        }
        else
        {
            buffer.push_back(message);
            response = message;
        }
    }
    else if (message == WaitingFor)
    {
        response = message;
        output.push_back(WaitingFor);
        WaitingFor += 1;

        auto msg = std::find(buffer.begin(), buffer.end(), WaitingFor);

        while (msg != buffer.end())
        {
            output.push_back(*msg);
            buffer.erase(msg);
            WaitingFor += 1;
            msg = std::find(buffer.begin(), buffer.end(), WaitingFor);
        }
    }

    return response;
}

int Recipient::ReceiveMessageAlternative(int message)
{
    static std::uniform_real_distribution<float> dist(0.f, 1.f);

    // Russian roulette algorithm
    int response = 0;

    if (message < 0)
    {
        response = message;
    }
    else if (message != WaitingFor)
    {
        if (dist(rng) < 0.01)
        {
            if (dist(rng) < 0.5)
            {
                L += 1;
            } else
            {
                L -= 1;
            }
        }

        if (buffer.size() == L || (dist(rng) < 0.25))
        {
            response = -message;
        }
        else
        {
            buffer.push_back(message);
            response = message;
        }
    }
    else if (message == WaitingFor)
    {
        response = message;
        output.push_back(WaitingFor);
        WaitingFor += 1;

        auto msg = std::find(buffer.begin(), buffer.end(), WaitingFor);

        while (msg != buffer.end())
        {
            output.push_back(*msg);
            buffer.erase(msg);
            WaitingFor += 1;
            msg = std::find(buffer.begin(), buffer.end(), WaitingFor);
        }
    }

    return response;
}

int Recipient::ReceiveMessageFlex(int message)
{
    int response = 0;

    if (message < 0)
    {
        errorNumber++;
    }
    totalNumber++;

    double P = errorNumber / (double) totalNumber;

    if (message == 1000)
    {
        int mult = P < 0.1 ? 4 : P < 0.2 ? 2 : 1;
        L = static_cast<int>(P * mult * 24);
    }

    if (message < 0)
    {
        response = message;
    }
    else if (message != WaitingFor)
    {
        if (buffer.size() == L)
        {
            response = -message;
        }
        else
        {
            buffer.push_back(message);
            response = message;
        }
    }
    else if (message == WaitingFor)
    {
        response = message;
        output.push_back(WaitingFor);
        WaitingFor += 1;

        auto msg = std::find(buffer.begin(), buffer.end(), WaitingFor);

        while (msg != buffer.end())
        {
            output.push_back(*msg);
            buffer.erase(msg);
            WaitingFor += 1;
            msg = std::find(buffer.begin(), buffer.end(), WaitingFor);
        }
    }

    return response;
}
