#ifndef SELREP_RECIPIENT_H
#define SELREP_RECIPIENT_H
#include <iostream>
#include <vector>
#include <algorithm>

class Recipient
{

    std::vector<int> buffer;
    std::vector<int> output;
    int L;
public:
    explicit Recipient(int _L) : L(_L) {rng.seed(std::random_device()());}

    int ReceiveMessage(int message);

    int ReceiveMessageAlternative(int message);

    int ReceiveMessageFlex(int message);

    int WaitingFor = 1;
    int totalNumber = 0;
    int errorNumber = 0;

    std::mt19937 rng;

};


#endif
